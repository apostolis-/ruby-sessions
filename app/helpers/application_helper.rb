module ApplicationHelper
  def user_email(user)
    "<p>#{user.email}</p>" if user && user.email
  end
end
