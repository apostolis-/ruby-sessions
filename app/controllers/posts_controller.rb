class PostsController < ApplicationController
  def index
    @user = User.first

    respond_to do |format|
      format.html do
        @post = Post.new #-> gia tin for form pou zei stin index
        @posts = Post.all.order(created_at: :desc)
      end
      format.json do
        @posts = Post.all.order(created_at: :desc)
        render json: @posts # koitaei apo pio modelo erxetai to @posts
      end
    end
  end

  def show
    @post = Post.find(params[:id])
    @comment = Comment.new # not persisted, just to initiate, enw i create einai ayto poy vlepoyme sti vasi
  end

  def edit
    @post = Post.find(params[:id])
    # i edit einai auto poy vlepoyme, i update einai i kainoyria edit poy eiani sti vasi
  end

  def update
    @post = Post.find(params[:id])
    @post.update(post_params)

    redirect_to '/posts'
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.create(post_params)

    if @post.persisted? # oxi @post.save epistrefei true or false
      respond_to do |format|
        format.html do
          redirect_to posts_path, notice: "Post succesfully created!" # <- use helper for paths
        end

        format.js do
          @posts = Post.all.order(created_at: :desc)
        end
      end
    end
  end

  def destroy
    post = Post.find(params[:id]) # den xreiazetai to @post giati to diagrafoume, den 8a to 3anazitisoyme
    post.destroy # an eixe delete 8a pigaine kai 8a to esvine apo ti vasi, to destroy akolou8ei ta assosionations. px an eixe comments to post, kai kaname delete to post, ta comments 8a mename orfana. Me to "destroy" svinoun kai ta comments(dependent: :destroy)

    # if post.destroy
    #   redirect_to '/posts' # redirect_to posts_path # zitaw ena action me to direct, enw me to render zitaw ena view
    # end

    # if js/ajax
    respond_to do |format| # to respond einai san to done tis js enos ajax request
      format.html do
        redirect_to posts_path
      end

      format.js do
        @posts = Post.all.order(created_at: :desc)
      end
    end
  end

  private # or protect (apo tin klassi poy eimaste kai katw), auti i methodos einai prosvasimi mono apo to idio to instance toy sugkekrimenou object

  def post_params
    params.require(:post).permit(:title, :text) # an kalesoume tin post_params 8a mas epistrepsei mono ton titlo kai to text pou exei vrei mesa sto params[title], params[text]
    # params.require(:post).permit!
  end
end
