class ContactController < ApplicationController
  def index
  end

  def send_email
    ApplicationMailer.contact_form(params["Email"], params["Name"], params["Message"]).deliver
    redirect_to contact_path, notice: "Message sent!"
  end
end
