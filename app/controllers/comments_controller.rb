class CommentsController < ApplicationController
  # hooks
  before_action :fetch_comments, only: [:index, :toggle_status]

  def create
    # to post.id mporoume na to valoume kai sti forma
    # i mporoume na to valoume mesa sti new (posts controller)
    @comment = Comment.create(comment_params)
  end

  def index
    # @comments = Comment.all <- trexei apo to before_action
  end

  def toggle_status
    # ta rails sto telos tis methodou kaloun tin render kai prospa8ei na vrei to view,
    # edw tou leme specific kane render ena js code
    # render js: "alert('ok')"

    # @comments = Comment.all <- trexei apo to before_action

    # meta to action ston controller, prepei to model na alla3ei to status tis vasis (approved i oxi)

    # vriskw to comment
    @comment = Comment.find(params[:id])
    # @comment.approved = !@comment.approved
    @comment.toggle!(:approved)
  end

  private
  def comment_params
    params.require(:comment).permit(:author, :body, :post_id)
  end

  def fetch_comments
    @comments = Comment.all
  end
end

# klikarw
# pairnw xhr
# apantaei o controller me JS (rendearei ena view)
# sto js allazoume to dom element
# paei sti toggle_status
# vriskoume to comment
# before_action trexei prin tis methodous tou controller
#
