class Api::PostsController < Api::BaseController
  def index
    # den xreiazetai to @posts , dn exoume view
    render json: Post.all.order(created_at: :desc), root: true, each_serializer: PostSerializer # <- vgainei ena epipedo e3w
  end
end
