class Api::BaseController < ActionController::Metal # ::Metal ειναι modules
  include AbstractController::Rendering
  include ActionView::Rendering
  include ActionController::Rendering
  include ActionController::Renderers::All
  include ActionController::Serialization
end
