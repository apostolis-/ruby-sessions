class UsersController < ApplicationController
  before_action :authenticate_user! # user is the resource

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.create(user_params)

    if @user.persisted? # oxi @user.save epistrefei true or false
      respond_to do |format|
        format.html do
          redirect_to user_path, notice: "User succesfully created!" # <- use helper for paths
        end

        format.js do
          @users = User.all.order(created_at: :desc)
        end
      end
    end
  end
end
