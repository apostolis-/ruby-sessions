class PostSerializer < ActiveModel::Serializer
  # douleuei san ton decorator
  # attributes pou 8eloume na dei3oume apo ta posts
  attributes :id, :title, :created_date

  def created_date
    object.created_at.strftime("%d-%m-%Y") # einai san ton this tis js giati den 3erei to @posts
  end
end
