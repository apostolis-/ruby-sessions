class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'

  def contact_form(email, name, message)
    @email = email
    @name = name
    @message = message
    mail(to: "a.taxidaridis@gmail.com", subject: "text email")
  end
end
