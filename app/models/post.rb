class Post < ApplicationRecord
  has_many :comments
  belongs_to :user

  validates :title, presence: true

  def is_new?
   created_at >= Date.today
  end
end
