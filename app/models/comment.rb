class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user

  scope :latest, -> { order(updated_at: :desc) }
  scope :approved, -> { where(approved: true) }

  def approved?
    approved
  end
end

# Comment.all.each do |comment|
#   comment.update_attributes(approved: false) unless comment.approved?
# end
