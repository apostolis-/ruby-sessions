Rails.application.routes.draw do
  devise_for :users
  root 'welcome#index' # change from get to root and the "/" to "#", changes the default index page
  resources :posts
  resources :users, only: [:index]
  resources :comments, only: [:create, :index]

  get  '/contact'      => 'contact#index'
  post '/contact/send' => 'contact#send_email'

  get '/comments/:id/toggle_status' => 'comments#toggle_status', as: :comment_toggle_status

  # kalw mia me8odo, ara 8elw ena endpoint, alla dn 8elw na fortwnei ena adeio template, opote
  # anti gia html kanw xhr is js

  # namespace gia ton api::posts_controller
  namespace :api do
    resources :posts
  end

  namespace :admin do
    resources :posts
  end
end
