class AddAuthorToComments < ActiveRecord::Migration[5.1]
  def change
    add_column :comments, :author, :string, after: :id
  end
end
